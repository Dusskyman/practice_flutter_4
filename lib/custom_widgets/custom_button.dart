import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomButton extends StatefulWidget {
  final AnimationController animationController;
  Color color;
  CustomButton(
    this.animationController, {
    this.color,
  });

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      height: 100,
      margin: EdgeInsets.only(bottom: 100),
      child: Material(
        color: widget.color,
        borderRadius: BorderRadius.circular(20),
        child: InkWell(
          splashColor: Colors.blue,
          onTap: () {
            if (widget.animationController.isCompleted) {
              widget.animationController.reverse();
            } else {
              widget.animationController.forward();
            }
          },
          child: Center(
            child: Text(
              'TAP ME',
              style: TextStyle(fontSize: 40),
            ),
          ),
        ),
      ),
    );
  }
}
