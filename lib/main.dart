import 'package:flutter/material.dart';

import 'custom_widgets/custom_button.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  CurvedAnimation curvedAnimation;
  Animation<Color> animation;
  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
      reverseDuration: Duration(milliseconds: 300),
    );
    curvedAnimation = CurvedAnimation(
      parent: animationController,
      curve: Curves.bounceOut,
      reverseCurve: Curves.easeIn,
    );
    animation = ColorTween(
      begin: Colors.amber,
      end: Colors.blue,
    ).animate(animationController);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepOrange,
      body: AnimatedBuilder(
        animation: curvedAnimation,
        builder: (context, child) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.center,
                width: double.infinity,
                height: curvedAnimation.value * 200,
                color: animation.value,
                child: Text(
                  'HELLO',
                  style: TextStyle(fontSize: 40),
                ),
              ),
              CustomButton(
                animationController,
                color: animation.value,
              ),
            ],
          );
        },
      ),
    );
  }
}
